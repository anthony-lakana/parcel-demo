* SETUP
* * $ sudo npm install -g parcel parcel-bundler jest jest-enzyme && npm install && npm run tdd

* Create a NEW THEME
* * $ npm run theme < Name Your New Theme >
* * $ cd < Name Your New Theme >

* NPM COMMANDS
* * $ npm run server - launches live browser view of page
* * $ npm run test - runs jest
* * $ npm tdd - runs server & test

* File System
* * < theme > / dist - distriubution folder
* * base-theme - template used to build other themes.
* * _dependencies - dependencies that are common amongst themes

* Dependencies
* * jQuery - loaded from node_modules & scoped locally to avoid conflict
* * YUI - loaded from node_modules & scoped locally to avoid conflict
* * AlloyUI - loaded from node_modules & scoped locally to avoid conflict
